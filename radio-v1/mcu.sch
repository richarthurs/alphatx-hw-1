EESchema Schematic File Version 4
LIBS:radio-v1-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L radio-library:SW_Push SW1
U 1 1 5EC980B8
P 3450 4800
AR Path="/5EC980B8" Ref="SW1"  Part="1" 
AR Path="/5EC971DB/5EC980B8" Ref="SW1"  Part="1" 
F 0 "SW1" H 3450 5085 50  0000 C CNN
F 1 "SW_Push" H 3450 4994 50  0000 C CNN
F 2 "radio-footprints:tact-switch-4" H 3450 5000 50  0001 C CNN
F 3 "" H 3450 5000 50  0001 C CNN
	1    3450 4800
	1    0    0    -1  
$EndComp
Text Label 2900 4800 0    50   ~ 0
NRST
$Comp
L radio-library:C_Small C11
U 1 1 5EC9833C
P 3200 5050
F 0 "C11" H 3292 5096 50  0000 L CNN
F 1 "0.1uF" H 3292 5005 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 3200 5050 50  0001 C CNN
F 3 "~" H 3200 5050 50  0001 C CNN
	1    3200 5050
	-1   0    0    1   
$EndComp
Text Label 7500 1200 0    50   ~ 0
NRST
Wire Wire Line
	8000 1200 7500 1200
$Comp
L power:GND #PWR0101
U 1 1 5EC9868B
P 8650 4250
F 0 "#PWR0101" H 8650 4000 50  0001 C CNN
F 1 "GND" H 8655 4077 50  0000 C CNN
F 2 "" H 8650 4250 50  0001 C CNN
F 3 "" H 8650 4250 50  0001 C CNN
	1    8650 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 4150 8800 4000
Wire Wire Line
	8500 4150 8500 4000
Wire Wire Line
	8600 4000 8600 4150
Connection ~ 8600 4150
Wire Wire Line
	8600 4150 8500 4150
Wire Wire Line
	8700 4000 8700 4150
Connection ~ 8700 4150
Wire Wire Line
	8700 4150 8800 4150
$Comp
L power:+3V3 #PWR0102
U 1 1 5EC98D7C
P 8700 800
F 0 "#PWR0102" H 8700 650 50  0001 C CNN
F 1 "+3V3" H 8650 950 50  0000 C CNN
F 2 "" H 8700 800 50  0001 C CNN
F 3 "" H 8700 800 50  0001 C CNN
	1    8700 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8700 800  8800 800 
Wire Wire Line
	8800 800  8800 1000
Wire Wire Line
	8600 1000 8600 800 
Wire Wire Line
	8600 800  8700 800 
Connection ~ 8700 800 
Wire Wire Line
	8700 800  8700 1000
Text Label 7500 1600 0    50   ~ 0
OSC_IN
Text Label 7500 1700 0    50   ~ 0
OSC_OUT
Wire Wire Line
	8000 1600 7500 1600
Wire Wire Line
	7500 1700 8000 1700
Text Notes 2900 4150 0    50   ~ 10
MCU Reset
Text Notes 5300 5800 0    50   ~ 10
MCU Oscillator
$Comp
L radio-library:C_Small C12
U 1 1 5EC99D00
P 5750 6850
F 0 "C12" H 5500 6900 50  0000 L CNN
F 1 "14pF" H 5350 6800 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 5750 6850 50  0001 C CNN
F 3 "~" H 5750 6850 50  0001 C CNN
	1    5750 6850
	1    0    0    -1  
$EndComp
$Comp
L radio-library:C_Small C13
U 1 1 5EC99D59
P 6350 6850
F 0 "C13" H 6442 6896 50  0000 L CNN
F 1 "14pF" H 6442 6805 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 6350 6850 50  0001 C CNN
F 3 "~" H 6350 6850 50  0001 C CNN
	1    6350 6850
	1    0    0    -1  
$EndComp
Text Label 5750 6300 1    50   ~ 0
OSC_IN
Text Label 6350 6350 1    50   ~ 0
OSC_OUT
Wire Wire Line
	5750 6950 5750 7100
Wire Wire Line
	5750 7100 6050 7100
Wire Wire Line
	6350 7100 6350 6950
$Comp
L power:GND #PWR0103
U 1 1 5EC9DBAA
P 3900 5200
F 0 "#PWR0103" H 3900 4950 50  0001 C CNN
F 1 "GND" H 3905 5027 50  0000 C CNN
F 2 "" H 3900 5200 50  0001 C CNN
F 3 "" H 3900 5200 50  0001 C CNN
	1    3900 5200
	1    0    0    -1  
$EndComp
Text Notes 650  5800 0    50   ~ 10
MCU Decoupling
Text Notes 650  7600 0    50   Italic 0
Note: place close to each VDD/VSS pair.
$Comp
L radio-library:C_Small C3
U 1 1 5EC9F643
P 1250 6250
F 0 "C3" H 1342 6296 50  0000 L CNN
F 1 "100 nF" H 1342 6205 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 1250 6250 50  0001 C CNN
F 3 "~" H 1250 6250 50  0001 C CNN
	1    1250 6250
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0104
U 1 1 5ECA052A
P 1250 6100
F 0 "#PWR0104" H 1250 5950 50  0001 C CNN
F 1 "+3V3" H 1265 6273 50  0000 C CNN
F 2 "" H 1250 6100 50  0001 C CNN
F 3 "" H 1250 6100 50  0001 C CNN
	1    1250 6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5ECA058A
P 1250 6400
F 0 "#PWR0105" H 1250 6150 50  0001 C CNN
F 1 "GND" H 1255 6227 50  0000 C CNN
F 2 "" H 1250 6400 50  0001 C CNN
F 3 "" H 1250 6400 50  0001 C CNN
	1    1250 6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 6100 1250 6150
Wire Wire Line
	1250 6350 1250 6400
$Comp
L radio-library:C_Small C1
U 1 1 5ECA5DD4
P 750 6250
F 0 "C1" H 842 6296 50  0000 L CNN
F 1 "100 nF" H 842 6205 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 750 6250 50  0001 C CNN
F 3 "~" H 750 6250 50  0001 C CNN
	1    750  6250
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0108
U 1 1 5ECA5DDA
P 750 6100
F 0 "#PWR0108" H 750 5950 50  0001 C CNN
F 1 "+3V3" H 765 6273 50  0000 C CNN
F 2 "" H 750 6100 50  0001 C CNN
F 3 "" H 750 6100 50  0001 C CNN
	1    750  6100
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5ECA5DE0
P 750 6400
F 0 "#PWR0109" H 750 6150 50  0001 C CNN
F 1 "GND" H 755 6227 50  0000 C CNN
F 2 "" H 750 6400 50  0001 C CNN
F 3 "" H 750 6400 50  0001 C CNN
	1    750  6400
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  6100 750  6150
Wire Wire Line
	750  6350 750  6400
$Comp
L radio-library:C_Small C2
U 1 1 5ECA646C
P 750 7100
F 0 "C2" H 842 7146 50  0000 L CNN
F 1 "10 uF" H 842 7055 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 750 7100 50  0001 C CNN
F 3 "~" H 750 7100 50  0001 C CNN
	1    750  7100
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0110
U 1 1 5ECA6472
P 750 6950
F 0 "#PWR0110" H 750 6800 50  0001 C CNN
F 1 "+3V3" H 765 7123 50  0000 C CNN
F 2 "" H 750 6950 50  0001 C CNN
F 3 "" H 750 6950 50  0001 C CNN
	1    750  6950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0111
U 1 1 5ECA6478
P 750 7250
F 0 "#PWR0111" H 750 7000 50  0001 C CNN
F 1 "GND" H 755 7077 50  0000 C CNN
F 2 "" H 750 7250 50  0001 C CNN
F 3 "" H 750 7250 50  0001 C CNN
	1    750  7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  6950 750  7000
Wire Wire Line
	750  7200 750  7250
$Comp
L radio-library:C_Small C4
U 1 1 5ECA8B6C
P 1250 7100
F 0 "C4" H 1342 7146 50  0000 L CNN
F 1 "10 uF" H 1342 7055 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 1250 7100 50  0001 C CNN
F 3 "~" H 1250 7100 50  0001 C CNN
	1    1250 7100
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0112
U 1 1 5ECA8B72
P 1250 6950
F 0 "#PWR0112" H 1250 6800 50  0001 C CNN
F 1 "+3V3" H 1265 7123 50  0000 C CNN
F 2 "" H 1250 6950 50  0001 C CNN
F 3 "" H 1250 6950 50  0001 C CNN
	1    1250 6950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5ECA8B78
P 1250 7250
F 0 "#PWR0113" H 1250 7000 50  0001 C CNN
F 1 "GND" H 1255 7077 50  0000 C CNN
F 2 "" H 1250 7250 50  0001 C CNN
F 3 "" H 1250 7250 50  0001 C CNN
	1    1250 7250
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 6950 1250 7000
Wire Wire Line
	1250 7200 1250 7250
$Comp
L power:+3V3 #PWR0120
U 1 1 5ECAE87E
P 8900 800
F 0 "#PWR0120" H 8900 650 50  0001 C CNN
F 1 "+3V3" H 8900 950 50  0000 C CNN
F 2 "" H 8900 800 50  0001 C CNN
F 3 "" H 8900 800 50  0001 C CNN
	1    8900 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	8900 800  8900 1000
Text Label 8500 950  1    50   ~ 10
VBAT
$Comp
L power:+3V3 #PWR0121
U 1 1 5ECB0449
P 750 4600
F 0 "#PWR0121" H 750 4450 50  0001 C CNN
F 1 "+3V3" H 765 4773 50  0000 C CNN
F 2 "" H 750 4600 50  0001 C CNN
F 3 "" H 750 4600 50  0001 C CNN
	1    750  4600
	1    0    0    -1  
$EndComp
Text Label 1150 4650 1    50   ~ 10
VBAT_INPUT
Text Notes 650  4150 0    50   ~ 10
VBAT Supply Selection
$Comp
L radio-library:R_Small_US R1
U 1 1 5ECB07A0
P 750 4850
F 0 "R1" H 818 4896 50  0000 L CNN
F 1 "0 Ω" H 818 4805 50  0000 L CNN
F 2 "radio-footprints:R_0402" H 750 4850 50  0001 C CNN
F 3 "~" H 750 4850 50  0001 C CNN
	1    750  4850
	1    0    0    -1  
$EndComp
$Comp
L radio-library:R_Small_US R1*1
U 1 1 5ECB223E
P 1150 4850
F 0 "R1*1" H 1218 4896 50  0000 L CNN
F 1 "0 Ω" H 1218 4805 50  0000 L CNN
F 2 "radio-footprints:R_0402" H 1150 4850 50  0001 C CNN
F 3 "~" H 1150 4850 50  0001 C CNN
	1    1150 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  4600 750  4750
Wire Wire Line
	1150 4650 1150 4750
Text Label 1300 3050 3    50   ~ 10
VBAT_INPUT
$Comp
L power:GND #PWR0122
U 1 1 5ECB4BF0
P 1750 4850
F 0 "#PWR0122" H 1750 4600 50  0001 C CNN
F 1 "GND" H 1755 4677 50  0000 C CNN
F 2 "" H 1750 4850 50  0001 C CNN
F 3 "" H 1750 4850 50  0001 C CNN
	1    1750 4850
	1    0    0    -1  
$EndComp
$Comp
L radio-library:C_Small C7
U 1 1 5ECB4CE5
P 1750 4700
F 0 "C7" H 1842 4746 50  0000 L CNN
F 1 "100 nF" H 1842 4655 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 1750 4700 50  0001 C CNN
F 3 "~" H 1750 4700 50  0001 C CNN
	1    1750 4700
	1    0    0    -1  
$EndComp
$Comp
L radio-library:C_Small C10
U 1 1 5ECB4EA2
P 2200 4700
F 0 "C10" H 2292 4746 50  0000 L CNN
F 1 "10 uF" H 2292 4655 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 2200 4700 50  0001 C CNN
F 3 "~" H 2200 4700 50  0001 C CNN
	1    2200 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  4950 750  5000
Wire Wire Line
	1150 5000 1150 4950
Wire Wire Line
	750  5000 950  5000
Text Label 1200 5100 0    50   ~ 10
VBAT
Wire Wire Line
	1200 5100 950  5100
Wire Wire Line
	950  5100 950  5000
Connection ~ 950  5000
Wire Wire Line
	950  5000 1150 5000
Text Label 1750 4500 1    50   ~ 10
VBAT
Text Label 2200 4500 1    50   ~ 10
VBAT
Wire Wire Line
	1750 4500 1750 4600
Wire Wire Line
	2200 4500 2200 4600
$Comp
L power:GND #PWR0123
U 1 1 5ECC4559
P 2200 4850
F 0 "#PWR0123" H 2200 4600 50  0001 C CNN
F 1 "GND" H 2205 4677 50  0000 C CNN
F 2 "" H 2200 4850 50  0001 C CNN
F 3 "" H 2200 4850 50  0001 C CNN
	1    2200 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 4800 1750 4850
Wire Wire Line
	2200 4800 2200 4850
Text Notes 650  5450 0    50   Italic 0
Note: only populate one resistor.\nNote: resistor is used to choose between external \nVBAT source and onboard VBAT source for the RTC.
Text Label 7500 1400 0    50   ~ 0
BOOT0
Text Label 7500 2500 0    50   ~ 0
BOOT1
Wire Wire Line
	8000 2500 7500 2500
Text Label 3650 6550 0    50   ~ 0
BOOT0
$Comp
L power:GND #PWR0124
U 1 1 5ECC9D19
P 2950 6650
F 0 "#PWR0124" H 2950 6400 50  0001 C CNN
F 1 "GND" H 2955 6477 50  0000 C CNN
F 2 "" H 2950 6650 50  0001 C CNN
F 3 "" H 2950 6650 50  0001 C CNN
	1    2950 6650
	1    0    0    -1  
$EndComp
$Comp
L radio-library:R_Small_US R4
U 1 1 5ECCF596
P 3450 6550
F 0 "R4" V 3245 6550 50  0000 C CNN
F 1 "10k" V 3336 6550 50  0000 C CNN
F 2 "radio-footprints:R_0402" H 3450 6550 50  0001 C CNN
F 3 "~" H 3450 6550 50  0001 C CNN
	1    3450 6550
	0    1    1    0   
$EndComp
Wire Wire Line
	3550 6550 3650 6550
Wire Wire Line
	2950 6650 2950 6550
Text Label 4750 6550 0    50   ~ 0
BOOT1
$Comp
L power:GND #PWR0126
U 1 1 5ECD98B5
P 4050 6650
F 0 "#PWR0126" H 4050 6400 50  0001 C CNN
F 1 "GND" H 4055 6477 50  0000 C CNN
F 2 "" H 4050 6650 50  0001 C CNN
F 3 "" H 4050 6650 50  0001 C CNN
	1    4050 6650
	1    0    0    -1  
$EndComp
$Comp
L radio-library:R_Small_US R7
U 1 1 5ECD98CD
P 4550 6550
F 0 "R7" V 4345 6550 50  0000 C CNN
F 1 "10k" V 4436 6550 50  0000 C CNN
F 2 "radio-footprints:R_0402" H 4550 6550 50  0001 C CNN
F 3 "~" H 4550 6550 50  0001 C CNN
	1    4550 6550
	0    1    1    0   
$EndComp
Wire Wire Line
	4650 6550 4750 6550
Wire Wire Line
	4050 6650 4050 6550
Text Notes 2850 7600 0    50   Italic 0
Default: BOOT0 = 0, BOOT1 = 0\nPopulate only one of each resistor.
Text Notes 2850 5800 0    50   ~ 10
MCU Boot Selection
Wire Wire Line
	8000 1400 7500 1400
Text Label 9600 2300 0    50   ~ 0
BLINKY_R_n
$Comp
L radio-library:R_Small_US R9
U 1 1 5ECE21BD
P 6200 5100
F 0 "R9" V 6400 5100 50  0000 C CNN
F 1 "1k" V 6300 5100 50  0000 C CNN
F 2 "radio-footprints:R_0402" H 6200 5100 50  0001 C CNN
F 3 "~" H 6200 5100 50  0001 C CNN
	1    6200 5100
	0    1    1    0   
$EndComp
$Comp
L power:+3V3 #PWR0128
U 1 1 5ECE4485
P 5400 4900
F 0 "#PWR0128" H 5400 4750 50  0001 C CNN
F 1 "+3V3" H 5415 5073 50  0000 C CNN
F 2 "" H 5400 4900 50  0001 C CNN
F 3 "" H 5400 4900 50  0001 C CNN
	1    5400 4900
	1    0    0    -1  
$EndComp
Text Notes 5400 4400 0    50   ~ 10
Blinky LED\nR = PA0\nG = PA1\nB = PB5
Text Label 1200 1750 0    50   ~ 0
SWDIO
Text Label 1200 1850 0    50   ~ 0
SWCLK
Text HLabel 9600 2500 2    50   Input ~ 0
MCU_TX
Text HLabel 9600 2600 2    50   Input ~ 0
MCU_RX
Wire Wire Line
	8600 4150 8650 4150
Wire Wire Line
	8650 4150 8650 4250
Connection ~ 8650 4150
Wire Wire Line
	8650 4150 8700 4150
$Comp
L power:GND #PWR0132
U 1 1 5ECA1192
P 6050 7200
F 0 "#PWR0132" H 6050 6950 50  0001 C CNN
F 1 "GND" H 6055 7027 50  0000 C CNN
F 2 "" H 6050 7200 50  0001 C CNN
F 3 "" H 6050 7200 50  0001 C CNN
	1    6050 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 7200 6050 7100
Connection ~ 6050 7100
Wire Wire Line
	6050 7100 6350 7100
Wire Notes Line
	600  5600 2600 5600
Wire Notes Line
	2600 5600 2600 7700
Wire Notes Line
	2600 7700 600  7700
Wire Notes Line
	600  7700 600  5600
Wire Notes Line
	2700 5600 2700 7700
Wire Notes Line
	2700 7700 5100 7700
Wire Notes Line
	5100 7700 5100 5600
Wire Notes Line
	5100 5600 2700 5600
Text Notes 5350 7600 0    50   Italic 0
Note: ground island for crystal caps.\nConnect to GND plane with one trace.
Wire Notes Line
	5200 5600 6900 5600
Wire Notes Line
	6900 5600 6900 7700
Wire Notes Line
	6900 7700 5200 7700
Wire Notes Line
	5200 7700 5200 5600
Text HLabel 9600 2700 2    50   Output ~ 0
SPI1_CSn
Text HLabel 9600 2800 2    50   Output ~ 0
SPI1_CLK
Text HLabel 9600 3000 2    50   Output ~ 0
SPI1_MOSI
Text HLabel 9600 2900 2    50   Input ~ 0
SPI1_MISO
Wire Wire Line
	9600 2700 9300 2700
Wire Wire Line
	9600 2800 9300 2800
Wire Wire Line
	9300 2900 9600 2900
Wire Wire Line
	9300 3000 9600 3000
Text Notes 10650 2900 2    50   Italic 0
> To CC1101
Text Label 7500 3800 0    50   ~ 0
SPI2_MOSI
Text Label 7500 3700 0    50   ~ 0
SPI2_MISO
Text Label 7500 3600 0    50   ~ 0
SPI2_CLK
Text Label 7500 3500 0    50   ~ 0
SPI2_CSn
Text Label 4150 3500 1    50   ~ 0
SPI2_MOSI
Text Label 4050 3500 1    50   ~ 0
SPI2_MISO
Text Label 3950 3500 1    50   ~ 0
SPI2_CLK
Text Label 3850 3500 1    50   ~ 0
SPI2_CSn
Wire Wire Line
	3850 2900 3850 3500
Wire Wire Line
	3950 3500 3950 2900
Wire Wire Line
	4050 2900 4050 3500
Wire Wire Line
	4150 3500 4150 2900
Text Notes 7450 3800 1    50   Italic 0
User SPI
Text Label 7500 3200 0    50   ~ 0
SDA
Text Label 7500 3100 0    50   ~ 0
SCL
Text Label 1700 3050 3    50   ~ 0
SCL
Text Label 1600 3050 3    50   ~ 0
SDA
Text Notes 4300 4150 0    50   ~ 10
I2C Pullups
Text Label 4550 5150 2    50   ~ 0
SDA
$Comp
L radio-library:R_Small_US R6
U 1 1 5ECC802C
P 4550 4900
F 0 "R6" V 4345 4900 50  0000 C CNN
F 1 "10k" V 4436 4900 50  0000 C CNN
F 2 "radio-footprints:R_0402" H 4550 4900 50  0001 C CNN
F 3 "~" H 4550 4900 50  0001 C CNN
	1    4550 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0133
U 1 1 5ECCF3DF
P 4550 4700
F 0 "#PWR0133" H 4550 4550 50  0001 C CNN
F 1 "+3V3" H 4565 4873 50  0000 C CNN
F 2 "" H 4550 4700 50  0001 C CNN
F 3 "" H 4550 4700 50  0001 C CNN
	1    4550 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4550 4700 4550 4800
Wire Wire Line
	4550 5000 4550 5150
Text Label 4850 5150 0    50   ~ 0
SCL
$Comp
L radio-library:R_Small_US R8
U 1 1 5ECDA303
P 4850 4900
F 0 "R8" V 4950 4900 50  0000 C CNN
F 1 "10k" V 5050 4900 50  0000 C CNN
F 2 "radio-footprints:R_0402" H 4850 4900 50  0001 C CNN
F 3 "~" H 4850 4900 50  0001 C CNN
	1    4850 4900
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0134
U 1 1 5ECDA309
P 4850 4700
F 0 "#PWR0134" H 4850 4550 50  0001 C CNN
F 1 "+3V3" H 4865 4873 50  0000 C CNN
F 2 "" H 4850 4700 50  0001 C CNN
F 3 "" H 4850 4700 50  0001 C CNN
	1    4850 4700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4850 4700 4850 4800
Wire Wire Line
	4850 5000 4850 5150
$Comp
L radio-library:R_Small_US R3
U 1 1 5ECF1D82
P 3200 4550
F 0 "R3" V 2995 4550 50  0000 C CNN
F 1 "10k" V 3086 4550 50  0000 C CNN
F 2 "radio-footprints:R_0402" H 3200 4550 50  0001 C CNN
F 3 "~" H 3200 4550 50  0001 C CNN
	1    3200 4550
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR0135
U 1 1 5ECF1DE2
P 3200 4400
F 0 "#PWR0135" H 3200 4250 50  0001 C CNN
F 1 "+3V3" H 3215 4573 50  0000 C CNN
F 2 "" H 3200 4400 50  0001 C CNN
F 3 "" H 3200 4400 50  0001 C CNN
	1    3200 4400
	1    0    0    -1  
$EndComp
Wire Wire Line
	2900 4800 3200 4800
Wire Wire Line
	3200 4650 3200 4800
Connection ~ 3200 4800
Wire Wire Line
	3200 4800 3250 4800
Wire Wire Line
	3200 4400 3200 4450
Wire Notes Line
	600  5500 600  4000
Wire Notes Line
	2800 4000 2800 5500
Wire Notes Line
	2800 5500 4100 5500
Wire Notes Line
	4100 5500 4100 4000
Wire Notes Line
	4100 4000 2800 4000
Wire Notes Line
	4200 5500 4200 4000
Wire Notes Line
	4200 4000 5200 4000
Wire Notes Line
	5200 4000 5200 5500
Wire Notes Line
	5200 5500 4200 5500
Wire Notes Line
	600  4000 2700 4000
Wire Notes Line
	2700 4000 2700 5500
Wire Notes Line
	2700 5500 600  5500
Wire Notes Line
	5300 5500 5300 4000
Text Notes 4250 5400 0    50   Italic 0
Note: place close to\nMCU pins.
Wire Wire Line
	8000 3100 7500 3100
Wire Wire Line
	7500 3200 8000 3200
Text HLabel 7900 3300 0    50   Input ~ 0
RF_GDO0
Text HLabel 9600 3100 2    50   Input ~ 0
RF_GDO2
Text HLabel 3350 3000 3    50   Input ~ 0
RF_GDO2
Text Label 9600 3600 0    50   ~ 0
SWDIO
Text Label 9600 3700 0    50   ~ 0
SWCLK
Wire Wire Line
	9600 3600 9300 3600
Wire Wire Line
	9300 3700 9600 3700
Wire Wire Line
	9600 2300 9300 2300
Wire Wire Line
	9300 2400 9600 2400
NoConn ~ 9300 3200
NoConn ~ 9300 3300
NoConn ~ 8000 3400
NoConn ~ 8000 2600
NoConn ~ 8000 2400
NoConn ~ 8000 2300
NoConn ~ 8000 2100
NoConn ~ 8000 2000
Wire Wire Line
	8500 950  8500 1000
Wire Wire Line
	8000 3500 7500 3500
Wire Wire Line
	7500 3600 8000 3600
Wire Wire Line
	8000 3700 7500 3700
Wire Wire Line
	7500 3800 8000 3800
$Comp
L radio-library:Conn_01x10 J4
U 1 1 5ECC245A
P 1600 2700
F 0 "J4" V 1817 2646 50  0000 C CNN
F 1 "Conn_01x10" V 1726 2646 50  0000 C CNN
F 2 "radio-footprints:Pin_Header_Straight_1x10_Pitch2.54mm" H 1600 2700 50  0001 C CNN
F 3 "~" H 1600 2700 50  0001 C CNN
	1    1600 2700
	0    -1   -1   0   
$EndComp
$Comp
L radio-library:Crystal Y1
U 1 1 5ECD2A29
P 6050 6550
F 0 "Y1" H 6050 6818 50  0000 C CNN
F 1 "Crystal" H 6050 6727 50  0000 C CNN
F 2 "radio-footprints:Crystal_SMD_SeikoEpson_TSX3225-4pin_3.2x2.5mm" H 6050 6550 50  0001 C CNN
F 3 "~" H 6050 6550 50  0001 C CNN
	1    6050 6550
	1    0    0    -1  
$EndComp
Wire Wire Line
	6350 6350 6350 6550
Wire Wire Line
	5750 6300 5750 6550
Wire Wire Line
	6250 6550 6350 6550
Connection ~ 6350 6550
Wire Wire Line
	6350 6550 6350 6750
Wire Wire Line
	5850 6550 5750 6550
Connection ~ 5750 6550
Wire Wire Line
	5750 6550 5750 6750
$Comp
L power:GND #PWR0171
U 1 1 5ECF2A02
P 5900 6700
F 0 "#PWR0171" H 5900 6450 50  0001 C CNN
F 1 "GND" H 5905 6527 50  0000 C CNN
F 2 "" H 5900 6700 50  0001 C CNN
F 3 "" H 5900 6700 50  0001 C CNN
	1    5900 6700
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0172
U 1 1 5ECF2A4B
P 6200 6700
F 0 "#PWR0172" H 6200 6450 50  0001 C CNN
F 1 "GND" H 6205 6527 50  0000 C CNN
F 2 "" H 6200 6700 50  0001 C CNN
F 3 "" H 6200 6700 50  0001 C CNN
	1    6200 6700
	1    0    0    -1  
$EndComp
Wire Wire Line
	5900 6700 5900 6650
Wire Wire Line
	6200 6650 6200 6700
NoConn ~ 3250 4900
NoConn ~ 3650 4900
Text Label 6350 4700 0    50   ~ 0
BLINKY_R_n
Wire Notes Line
	6900 5500 6900 4000
Wire Notes Line
	5300 5500 6900 5500
Wire Notes Line
	5300 4000 6900 4000
$Comp
L radio-library:STM32F103CBTx U1
U 1 1 5ECE0F21
P 8700 2500
F 0 "U1" H 8650 1500 50  0000 C CNN
F 1 "STM32F103CBTx" H 8650 1650 50  0000 C CNN
F 2 "radio-footprints:LQFP-48_7x7mm_Pitch0.5mm" H 8100 1100 50  0001 R CNN
F 3 "http://www.st.com/st-web-ui/static/active/en/resource/technical/document/datasheet/CD00161566.pdf" H 8700 2500 50  0001 C CNN
	1    8700 2500
	1    0    0    -1  
$EndComp
Wire Wire Line
	3650 4800 3900 4800
Wire Wire Line
	3900 4800 3900 5200
$Comp
L power:GND #PWR0129
U 1 1 5ECE4889
P 3200 5200
F 0 "#PWR0129" H 3200 4950 50  0001 C CNN
F 1 "GND" H 3205 5027 50  0000 C CNN
F 2 "" H 3200 5200 50  0001 C CNN
F 3 "" H 3200 5200 50  0001 C CNN
	1    3200 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3200 5150 3200 5200
Wire Wire Line
	3200 4800 3200 4950
Text GLabel 1100 1750 0    50   BiDi ~ 0
SWDIO
Text GLabel 1100 1850 0    50   Input ~ 0
SWCLK
Wire Wire Line
	1100 1750 1200 1750
Wire Wire Line
	1100 1850 1200 1850
Text Notes 700  1550 0    50   ~ 10
SWD Signals \nFrom J1
$Comp
L radio-library:Conn_01x10 J5
U 1 1 5EDC4E71
P 3650 2700
F 0 "J5" V 3867 2646 50  0000 C CNN
F 1 "Conn_01x10" V 3776 2646 50  0000 C CNN
F 2 "radio-footprints:Pin_Header_Straight_1x10_Pitch2.54mm" H 3650 2700 50  0001 C CNN
F 3 "~" H 3650 2700 50  0001 C CNN
	1    3650 2700
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4050 6550 4450 6550
Wire Wire Line
	2950 6550 3350 6550
Text HLabel 3450 3000 3    50   Output ~ 0
SPI1_CSn
Text HLabel 3550 3000 3    50   Output ~ 0
SPI1_CLK
Text HLabel 3650 3000 3    50   Input ~ 0
SPI1_MISO
Text HLabel 3750 3000 3    50   Output ~ 0
SPI1_MOSI
Wire Wire Line
	3450 2900 3450 3000
Wire Wire Line
	3550 2900 3550 3000
Wire Wire Line
	3650 2900 3650 3000
Wire Wire Line
	3750 2900 3750 3000
Text HLabel 3250 3000 3    50   Input ~ 0
RF_GDO0
Wire Wire Line
	3250 2900 3250 3000
Wire Wire Line
	3350 2900 3350 3000
$Comp
L power:GND #PWR0106
U 1 1 5EDDA6EC
P 1100 3300
F 0 "#PWR0106" H 1100 3050 50  0001 C CNN
F 1 "GND" H 1105 3127 50  0000 C CNN
F 2 "" H 1100 3300 50  0001 C CNN
F 3 "" H 1100 3300 50  0001 C CNN
	1    1100 3300
	1    0    0    -1  
$EndComp
Text Label 1400 3050 3    50   ~ 0
USART1_RX
Text Label 1500 3050 3    50   ~ 0
USART1_TX
Text Label 7900 3000 2    50   ~ 0
USART1_RX
Text Label 7900 2900 2    50   ~ 0
USART1_TX
Wire Wire Line
	9600 2600 9300 2600
Wire Wire Line
	9300 2500 9600 2500
Wire Wire Line
	1200 2900 1200 3200
Wire Wire Line
	1200 3200 1100 3200
Wire Wire Line
	1100 3200 1100 3300
Wire Wire Line
	1300 3050 1300 2900
Wire Wire Line
	1400 2900 1400 3050
Wire Wire Line
	1500 3050 1500 2900
Wire Wire Line
	1600 2900 1600 3050
Wire Wire Line
	1700 3050 1700 2900
Text Label 9600 3800 0    50   ~ 0
PA15_ADC
Wire Wire Line
	9600 3800 9300 3800
Text Label 1900 3050 3    50   ~ 0
PA15_ADC
Text Label 7500 2700 0    50   ~ 0
PB4_TIM3
Text Label 1800 3050 3    50   ~ 0
PB4_TIM3
Wire Wire Line
	1800 2900 1800 3050
Wire Wire Line
	1900 2900 1900 3050
Text Label 2100 3050 3    50   ~ 0
PA12_CAN_TIM1
Text Label 2000 3050 3    50   ~ 0
PA11_CAN_ADC_TIM1
Wire Wire Line
	9600 3400 9300 3400
Wire Wire Line
	9300 3500 9600 3500
Wire Wire Line
	2000 2900 2000 3050
Wire Wire Line
	2100 2900 2100 3050
Text Label 9600 3500 0    50   ~ 0
PA12_CAN_TIM1
Text Label 9600 3400 0    50   ~ 0
PA11_CAN_ADC_TIM1
Text Notes 6100 2350 0    50   ~ 0
Note: GPIO can sink/source\n8 mA.\n\nExcept PC13, PC14, PC15 can\nonly sink/source 3 mA.
Text Notes 10900 2550 2    50   Italic 0
> To FTDI (USART2)
Text Label 9600 2400 0    50   ~ 0
BLINKY_G_n
Text Notes 10100 2300 0    50   ~ 0
T2C1
Text Notes 10100 2400 0    50   ~ 0
T2C2
Text Label 7500 2800 0    50   ~ 0
BLINKY_B_n
Text Notes 7250 2800 0    50   ~ 0
T3C2
Wire Wire Line
	9300 3100 9600 3100
Wire Wire Line
	7900 3300 8000 3300
Text Notes 9600 4000 0    50   ~ 0
PA15: All functionality \navailable except for T2C1.
$Comp
L radio-library:EAST1616RGBA3 D1
U 1 1 5EE05F2D
P 5750 4900
F 0 "D1" H 5750 5300 50  0000 C CNN
F 1 "EAST1616RGBA3" H 5750 4500 50  0000 C CNN
F 2 "radio-footprints:EAST1616RGBA3" H 5750 4550 50  0001 C CNN
F 3 "file:///Users/Richard/Desktop/EAST1616RGBA3.PDF" H 5750 4850 50  0001 C CNN
	1    5750 4900
	1    0    0    -1  
$EndComp
Wire Wire Line
	5400 4900 5550 4900
Text Label 6350 4900 0    50   ~ 0
BLINKY_G_n
Text Label 6350 5100 0    50   ~ 0
BLINKY_B_n
$Comp
L radio-library:R_Small_US R5
U 1 1 5EE39EAA
P 6200 4900
F 0 "R5" V 6100 4900 50  0000 C CNN
F 1 "1k" V 6300 4900 50  0000 C CNN
F 2 "radio-footprints:R_0402" H 6200 4900 50  0001 C CNN
F 3 "~" H 6200 4900 50  0001 C CNN
	1    6200 4900
	0    1    1    0   
$EndComp
$Comp
L radio-library:R_Small_US R2
U 1 1 5EE39F03
P 6200 4700
F 0 "R2" V 5995 4700 50  0000 C CNN
F 1 "1k" V 6086 4700 50  0000 C CNN
F 2 "radio-footprints:R_0402" H 6200 4700 50  0001 C CNN
F 3 "~" H 6200 4700 50  0001 C CNN
	1    6200 4700
	0    1    1    0   
$EndComp
Wire Wire Line
	6300 4700 6350 4700
Wire Wire Line
	6300 4900 6350 4900
Wire Wire Line
	6300 5100 6350 5100
Wire Wire Line
	5950 4900 6100 4900
Wire Wire Line
	5950 4700 6100 4700
Wire Wire Line
	5950 5100 6100 5100
NoConn ~ 8000 1900
Wire Wire Line
	8000 2700 7500 2700
Wire Wire Line
	7500 2800 8000 2800
Wire Wire Line
	7900 2900 8000 2900
Wire Wire Line
	7900 3000 8000 3000
Wire Notes Line
	600  3900 600  2200
Wire Notes Line
	600  2200 2700 2200
Wire Notes Line
	2700 2200 2700 3900
Wire Notes Line
	2700 3900 600  3900
Wire Notes Line
	2800 3900 2800 2200
Wire Notes Line
	2800 2200 4600 2200
Wire Notes Line
	4600 2200 4600 3900
Wire Notes Line
	4600 3900 2800 3900
Text Notes 2900 2400 0    50   ~ 10
J5 User Connector
Text Notes 700  2400 0    50   ~ 10
J4 User Connector
Wire Notes Line
	600  1300 1700 1300
Wire Notes Line
	1700 1300 1700 2100
Wire Notes Line
	1700 2100 600  2100
Wire Notes Line
	600  2100 600  1300
$Comp
L radio-library:24FC16T-I_OT U5
U 1 1 5EDF3367
P 8650 5550
F 0 "U5" H 8650 5915 50  0000 C CNN
F 1 "24FC16T-I_OT" H 8650 5824 50  0000 C CNN
F 2 "radio-footprints:TSOT-23-5" H 8400 5800 50  0001 C CNN
F 3 "" H 8400 5800 50  0001 C CNN
	1    8650 5550
	1    0    0    -1  
$EndComp
$Comp
L radio-library:C_Small C5
U 1 1 5EDF36AA
P 9450 5750
F 0 "C5" H 9542 5796 50  0000 L CNN
F 1 "100 nF" H 9542 5705 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 9450 5750 50  0001 C CNN
F 3 "~" H 9450 5750 50  0001 C CNN
	1    9450 5750
	1    0    0    -1  
$EndComp
$Comp
L power:+3V3 #PWR03
U 1 1 5EDF36B0
P 9450 5600
F 0 "#PWR03" H 9450 5450 50  0001 C CNN
F 1 "+3V3" H 9465 5773 50  0000 C CNN
F 2 "" H 9450 5600 50  0001 C CNN
F 3 "" H 9450 5600 50  0001 C CNN
	1    9450 5600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR04
U 1 1 5EDF36B6
P 9450 5900
F 0 "#PWR04" H 9450 5650 50  0001 C CNN
F 1 "GND" H 9455 5727 50  0000 C CNN
F 2 "" H 9450 5900 50  0001 C CNN
F 3 "" H 9450 5900 50  0001 C CNN
	1    9450 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9450 5600 9450 5650
Wire Wire Line
	9450 5850 9450 5900
Text Label 7850 5650 0    50   ~ 0
SDA
Text Label 7850 5450 0    50   ~ 0
SCL
Wire Wire Line
	9100 5650 9450 5650
Connection ~ 9450 5650
$Comp
L power:GND #PWR01
U 1 1 5EE1268B
P 8150 5800
F 0 "#PWR01" H 8150 5550 50  0001 C CNN
F 1 "GND" H 8155 5627 50  0000 C CNN
F 2 "" H 8150 5800 50  0001 C CNN
F 3 "" H 8150 5800 50  0001 C CNN
	1    8150 5800
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 5550 8150 5550
Wire Wire Line
	8150 5550 8150 5800
Wire Wire Line
	7850 5650 8200 5650
Wire Wire Line
	7850 5450 8200 5450
$Comp
L power:GND #PWR02
U 1 1 5EE2332D
P 9200 5900
F 0 "#PWR02" H 9200 5650 50  0001 C CNN
F 1 "GND" H 9205 5727 50  0000 C CNN
F 2 "" H 9200 5900 50  0001 C CNN
F 3 "" H 9200 5900 50  0001 C CNN
	1    9200 5900
	1    0    0    -1  
$EndComp
Wire Wire Line
	9100 5450 9200 5450
Wire Wire Line
	9200 5450 9200 5900
Text Notes 8350 6250 0    50   Italic 0
Note: WP Disabled
Text Notes 5300 5950 0    50   Italic 0
Assuming 3 pF stray capacitence\nand 10 pF required load capacitence.
$EndSCHEMATC
