EESchema Schematic File Version 4
LIBS:radio-v1-cache
EELAYER 26 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 3 4
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 650  1850 0    50   Italic 0
3V3_EXT: 3.3 V supply from headers located by debug connection.\n3V3_REG_PROTECTED: onboard regulated 3.3 V powered by USB.\n
$Comp
L radio-library:FT230XS_QFN U3
U 1 1 5EC9902B
P 8850 3850
AR Path="/5EC9902B" Ref="U3"  Part="1" 
AR Path="/5EC971ED/5EC9902B" Ref="U3"  Part="1" 
F 0 "U3" H 8850 3800 50  0000 C CNN
F 1 "FT230XS" H 8850 3900 50  0000 C CNN
F 2 "radio-footprints:QFN-16-1EP_4x4mm_Pitch0.65mm" H 9300 3200 50  0001 C CNN
F 3 "http://www.ftdichip.com/Products/ICs/FT230X.html" H 8850 3850 50  0001 C CNN
	1    8850 3850
	1    0    0    -1  
$EndComp
Text Label 9700 2750 1    50   ~ 10
VCCIO
$Comp
L radio-library:R_Small_US R13
U 1 1 5ED4AFE2
P 9700 2950
F 0 "R13" H 9768 2996 50  0000 L CNN
F 1 "10k" H 9768 2905 50  0000 L CNN
F 2 "radio-footprints:R_0402" H 9700 2950 50  0001 C CNN
F 3 "~" H 9700 2950 50  0001 C CNN
	1    9700 2950
	1    0    0    -1  
$EndComp
$Comp
L radio-library:R_Small_US R14
U 1 1 5ED4B0C6
P 10000 2950
F 0 "R14" H 10068 2996 50  0000 L CNN
F 1 "10k" H 10068 2905 50  0000 L CNN
F 2 "radio-footprints:R_0402" H 10000 2950 50  0001 C CNN
F 3 "~" H 10000 2950 50  0001 C CNN
	1    10000 2950
	1    0    0    -1  
$EndComp
$Comp
L radio-library:R_Small_US R15
U 1 1 5ED4B121
P 10250 2950
F 0 "R15" H 10318 2996 50  0000 L CNN
F 1 "10k" H 10318 2905 50  0000 L CNN
F 2 "radio-footprints:R_0402" H 10250 2950 50  0001 C CNN
F 3 "~" H 10250 2950 50  0001 C CNN
	1    10250 2950
	1    0    0    -1  
$EndComp
$Comp
L radio-library:R_Small_US R16
U 1 1 5ED4B13C
P 10500 2950
F 0 "R16" H 10568 2996 50  0000 L CNN
F 1 "10k" H 10568 2905 50  0000 L CNN
F 2 "radio-footprints:R_0402" H 10500 2950 50  0001 C CNN
F 3 "~" H 10500 2950 50  0001 C CNN
	1    10500 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	9550 3450 9700 3450
Wire Wire Line
	9700 3450 9700 3050
Wire Wire Line
	9550 3550 10000 3550
Wire Wire Line
	10000 3550 10000 3050
Wire Wire Line
	9550 3650 10250 3650
Wire Wire Line
	10250 3650 10250 3050
Wire Wire Line
	9550 3750 10500 3750
Wire Wire Line
	10500 3750 10500 3050
Text Label 10000 2750 1    50   ~ 10
VCCIO
Text Label 10250 2750 1    50   ~ 10
VCCIO
Text Label 10500 2750 1    50   ~ 10
VCCIO
Wire Wire Line
	9700 2750 9700 2850
Wire Wire Line
	10000 2750 10000 2850
Wire Wire Line
	10250 2750 10250 2850
Wire Wire Line
	10500 2750 10500 2850
Connection ~ 9700 3450
Wire Wire Line
	9700 3450 10650 3450
Wire Wire Line
	10000 3550 10650 3550
Connection ~ 10000 3550
NoConn ~ 9550 3950
NoConn ~ 9550 4250
Text Label 9700 4150 0    50   ~ 0
RX_LED_SINK
Text Label 9700 4050 0    50   ~ 0
TX_LED_SINK
Wire Wire Line
	9550 4050 9700 4050
Wire Wire Line
	9550 4150 9700 4150
Text Label 7950 2100 2    50   ~ 0
TX_LED_SINK
$Comp
L radio-library:R_Small_US R18
U 1 1 5ED4C299
P 7950 1800
F 0 "R18" H 7800 1850 50  0000 L CNN
F 1 "1k" H 7800 1750 50  0000 L CNN
F 2 "radio-footprints:R_0402" H 7950 1800 50  0001 C CNN
F 3 "~" H 7950 1800 50  0001 C CNN
	1    7950 1800
	1    0    0    -1  
$EndComp
$Comp
L radio-library:LED D3
U 1 1 5ED4C318
P 7950 1400
F 0 "D3" V 8000 1600 50  0000 R CNN
F 1 "TX (Green)" V 7900 1900 50  0000 R CNN
F 2 "radio-footprints:LED_0603_HandSoldering" H 7950 1400 50  0001 C CNN
F 3 "~" H 7950 1400 50  0001 C CNN
	1    7950 1400
	0    -1   -1   0   
$EndComp
Text Label 7950 1100 1    50   ~ 10
VCCIO
Wire Wire Line
	7950 1550 7950 1700
Wire Wire Line
	7950 1900 7950 2100
Wire Wire Line
	7950 1250 7950 1100
Text Label 8400 2100 0    50   ~ 0
RX_LED_SINK
$Comp
L radio-library:R_Small_US R19
U 1 1 5ED4CC06
P 8400 1800
F 0 "R19" H 8468 1846 50  0000 L CNN
F 1 "1k" H 8468 1755 50  0000 L CNN
F 2 "radio-footprints:R_0402" H 8400 1800 50  0001 C CNN
F 3 "~" H 8400 1800 50  0001 C CNN
	1    8400 1800
	1    0    0    -1  
$EndComp
$Comp
L radio-library:LED D4
U 1 1 5ED4CC0C
P 8400 1400
F 0 "D4" V 8438 1283 50  0000 R CNN
F 1 "RX (Red)" V 8347 1283 50  0000 R CNN
F 2 "radio-footprints:LED_0603_HandSoldering" H 8400 1400 50  0001 C CNN
F 3 "~" H 8400 1400 50  0001 C CNN
	1    8400 1400
	0    -1   -1   0   
$EndComp
Text Label 8400 1100 1    50   ~ 10
VCCIO
Wire Wire Line
	8400 1550 8400 1700
Wire Wire Line
	8400 1900 8400 2100
Wire Wire Line
	8400 1250 8400 1100
Text Notes 7450 750  0    50   ~ 10
RX/TX LEDs
$Comp
L power:GND #PWR0136
U 1 1 5ED4D0E0
P 8650 4750
F 0 "#PWR0136" H 8650 4500 50  0001 C CNN
F 1 "GND" H 8655 4577 50  0000 C CNN
F 2 "" H 8650 4750 50  0001 C CNN
F 3 "" H 8650 4750 50  0001 C CNN
	1    8650 4750
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0137
U 1 1 5ED4D12D
P 8950 4750
F 0 "#PWR0137" H 8950 4500 50  0001 C CNN
F 1 "GND" H 8955 4577 50  0000 C CNN
F 2 "" H 8950 4750 50  0001 C CNN
F 3 "" H 8950 4750 50  0001 C CNN
	1    8950 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 4550 8650 4750
Wire Wire Line
	8950 4550 8950 4750
NoConn ~ 8150 4050
Text Label 8750 3050 2    50   ~ 10
USB_5V
Text Label 8950 3050 0    50   ~ 10
VCCIO
Wire Wire Line
	8950 3050 8950 3100
Wire Wire Line
	8750 3050 8750 3150
Wire Wire Line
	8150 3450 8100 3450
Text Label 7150 3850 0    50   ~ 0
USB+
Text Label 7150 3750 0    50   ~ 0
USB-
Wire Wire Line
	7800 3750 8150 3750
Wire Wire Line
	8150 3850 7800 3850
Wire Wire Line
	8950 3100 8100 3100
Wire Wire Line
	8100 3100 8100 3450
Connection ~ 8950 3100
Wire Wire Line
	8950 3100 8950 3150
$Comp
L radio-library:USB_B_Micro J3
U 1 1 5ED56EB3
P 1450 5100
F 0 "J3" H 1505 5567 50  0000 C CNN
F 1 "USB_B_Micro" H 1505 5476 50  0000 C CNN
F 2 "radio-footprints:USB-Micro-B-RA" H 1600 5050 50  0001 C CNN
F 3 "~" H 1600 5050 50  0001 C CNN
	1    1450 5100
	1    0    0    -1  
$EndComp
Text Label 3300 4900 0    50   ~ 10
USB_5V
Text Label 2000 5200 0    50   ~ 0
USB-
Text Label 2000 5100 0    50   ~ 0
USB+
Wire Wire Line
	2000 5100 1750 5100
Wire Wire Line
	1750 5200 2000 5200
$Comp
L power:GND #PWR0138
U 1 1 5ED59638
P 1450 6000
F 0 "#PWR0138" H 1450 5750 50  0001 C CNN
F 1 "GND" H 1455 5827 50  0000 C CNN
F 2 "" H 1450 6000 50  0001 C CNN
F 3 "" H 1450 6000 50  0001 C CNN
	1    1450 6000
	1    0    0    -1  
$EndComp
$Comp
L radio-library:C_Small C15
U 1 1 5ED596B9
P 1050 5750
F 0 "C15" H 1250 5700 50  0000 R CNN
F 1 "0.1 uF" H 1400 5800 50  0000 R CNN
F 2 "radio-footprints:C_0402" H 1050 5750 50  0001 C CNN
F 3 "~" H 1050 5750 50  0001 C CNN
	1    1050 5750
	-1   0    0    1   
$EndComp
$Comp
L radio-library:R_Small_US R10
U 1 1 5ED59731
P 1350 5750
F 0 "R10" H 1550 5700 50  0000 R CNN
F 1 "1M" H 1550 5800 50  0000 R CNN
F 2 "radio-footprints:R_0402" H 1350 5750 50  0001 C CNN
F 3 "~" H 1350 5750 50  0001 C CNN
	1    1350 5750
	-1   0    0    1   
$EndComp
Wire Wire Line
	1350 5500 1350 5600
Wire Wire Line
	1050 5650 1050 5600
Wire Wire Line
	1050 5600 1350 5600
Connection ~ 1350 5600
Wire Wire Line
	1350 5600 1350 5650
Wire Wire Line
	1050 5850 1050 5900
Wire Wire Line
	1050 5900 1200 5900
Wire Wire Line
	1350 5900 1350 5850
NoConn ~ 1750 5300
$Comp
L radio-library:Ferrite_Bead_Small FB1
U 1 1 5ED656AD
P 2700 4900
F 0 "FB1" V 2463 4900 50  0000 C CNN
F 1 "Ferrite" V 2554 4900 50  0000 C CNN
F 2 "radio-footprints:R_0603" V 2630 4900 50  0001 C CNN
F 3 "~" H 2700 4900 50  0001 C CNN
	1    2700 4900
	0    1    1    0   
$EndComp
$Comp
L radio-library:C_Small C18
U 1 1 5ED65767
P 2400 5100
F 0 "C18" H 2492 5146 50  0000 L CNN
F 1 "0.1 uF" H 2492 5055 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 2400 5100 50  0001 C CNN
F 3 "~" H 2400 5100 50  0001 C CNN
	1    2400 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 4900 2400 4900
Wire Wire Line
	2800 4900 3000 4900
Wire Wire Line
	2400 5000 2400 4900
$Comp
L power:GND #PWR0139
U 1 1 5ED6845E
P 2400 5300
F 0 "#PWR0139" H 2400 5050 50  0001 C CNN
F 1 "GND" H 2405 5127 50  0000 C CNN
F 2 "" H 2400 5300 50  0001 C CNN
F 3 "" H 2400 5300 50  0001 C CNN
	1    2400 5300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2400 5200 2400 5300
$Comp
L radio-library:R_Small_US R11
U 1 1 5EC9E17F
P 7700 3750
F 0 "R11" V 7650 3900 50  0000 C CNN
F 1 "27" V 7600 3750 50  0000 C CNN
F 2 "radio-footprints:R_0402" H 7700 3750 50  0001 C CNN
F 3 "~" H 7700 3750 50  0001 C CNN
	1    7700 3750
	0    1    1    0   
$EndComp
$Comp
L radio-library:C_Small C21
U 1 1 5EC9E364
P 7550 4150
F 0 "C21" H 7642 4196 50  0000 L CNN
F 1 "47 pF" H 7642 4105 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 7550 4150 50  0001 C CNN
F 3 "~" H 7550 4150 50  0001 C CNN
	1    7550 4150
	1    0    0    -1  
$EndComp
$Comp
L radio-library:R_Small_US R12
U 1 1 5EC9E44E
P 7700 3850
F 0 "R12" V 7750 4000 50  0000 C CNN
F 1 "27" V 7800 3850 50  0000 C CNN
F 2 "radio-footprints:R_0402" H 7700 3850 50  0001 C CNN
F 3 "~" H 7700 3850 50  0001 C CNN
	1    7700 3850
	0    1    1    0   
$EndComp
$Comp
L radio-library:C_Small C20
U 1 1 5EC9E6A0
P 7400 4150
F 0 "C20" H 7200 4200 50  0000 L CNN
F 1 "47 pF" H 7000 4100 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 7400 4150 50  0001 C CNN
F 3 "~" H 7400 4150 50  0001 C CNN
	1    7400 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7150 3850 7550 3850
Wire Wire Line
	7600 3750 7400 3750
Connection ~ 7400 3750
Wire Wire Line
	7400 3750 7150 3750
Connection ~ 7550 3850
Wire Wire Line
	7550 3850 7600 3850
$Comp
L power:GND #PWR0140
U 1 1 5ECA1E24
P 7400 4350
F 0 "#PWR0140" H 7400 4100 50  0001 C CNN
F 1 "GND" H 7405 4177 50  0000 C CNN
F 2 "" H 7400 4350 50  0001 C CNN
F 3 "" H 7400 4350 50  0001 C CNN
	1    7400 4350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0141
U 1 1 5ECA1E53
P 7550 4350
F 0 "#PWR0141" H 7550 4100 50  0001 C CNN
F 1 "GND" H 7555 4177 50  0000 C CNN
F 2 "" H 7550 4350 50  0001 C CNN
F 3 "" H 7550 4350 50  0001 C CNN
	1    7550 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	7550 4250 7550 4350
Wire Wire Line
	7400 4250 7400 4350
Wire Wire Line
	7550 3850 7550 4050
Wire Wire Line
	7400 3750 7400 4050
Text Label 1000 6900 2    50   ~ 10
VCCIO
$Comp
L radio-library:C_Small C14
U 1 1 5ECA79AD
P 1000 7100
F 0 "C14" H 800 7150 50  0000 L CNN
F 1 "4.7 uF" H 650 7050 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 1000 7100 50  0001 C CNN
F 3 "~" H 1000 7100 50  0001 C CNN
	1    1000 7100
	1    0    0    -1  
$EndComp
$Comp
L radio-library:C_Small C16
U 1 1 5ECA7A50
P 1300 7100
F 0 "C16" H 1392 7146 50  0000 L CNN
F 1 "0.1 uF" H 1392 7055 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 1300 7100 50  0001 C CNN
F 3 "~" H 1300 7100 50  0001 C CNN
	1    1300 7100
	1    0    0    -1  
$EndComp
Text Label 1300 6900 0    50   ~ 10
VCCIO
$Comp
L power:GND #PWR0142
U 1 1 5ECA7C2B
P 1300 7300
F 0 "#PWR0142" H 1300 7050 50  0001 C CNN
F 1 "GND" H 1305 7127 50  0000 C CNN
F 2 "" H 1300 7300 50  0001 C CNN
F 3 "" H 1300 7300 50  0001 C CNN
	1    1300 7300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0143
U 1 1 5ECA7CA4
P 1000 7300
F 0 "#PWR0143" H 1000 7050 50  0001 C CNN
F 1 "GND" H 1005 7127 50  0000 C CNN
F 2 "" H 1000 7300 50  0001 C CNN
F 3 "" H 1000 7300 50  0001 C CNN
	1    1000 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	1300 7200 1300 7300
Wire Wire Line
	1300 6900 1300 7000
Wire Wire Line
	1000 6900 1000 7000
Wire Wire Line
	1000 7200 1000 7300
Text Notes 700  6700 0    50   ~ 10
FT230 Regulator Stability
Text Label 2300 6900 2    50   ~ 10
USB_5V
$Comp
L radio-library:C_Small C17
U 1 1 5ECB00BD
P 2300 7100
F 0 "C17" H 2100 7150 50  0000 L CNN
F 1 "4.7 uF" H 1950 7050 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 2300 7100 50  0001 C CNN
F 3 "~" H 2300 7100 50  0001 C CNN
	1    2300 7100
	1    0    0    -1  
$EndComp
$Comp
L radio-library:C_Small C19
U 1 1 5ECB00C3
P 2600 7100
F 0 "C19" H 2692 7146 50  0000 L CNN
F 1 "0.1 uF" H 2692 7055 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 2600 7100 50  0001 C CNN
F 3 "~" H 2600 7100 50  0001 C CNN
	1    2600 7100
	1    0    0    -1  
$EndComp
Text Label 2600 6900 0    50   ~ 10
USB_5V
$Comp
L power:GND #PWR0144
U 1 1 5ECB00CA
P 2600 7300
F 0 "#PWR0144" H 2600 7050 50  0001 C CNN
F 1 "GND" H 2605 7127 50  0000 C CNN
F 2 "" H 2600 7300 50  0001 C CNN
F 3 "" H 2600 7300 50  0001 C CNN
	1    2600 7300
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0145
U 1 1 5ECB00D0
P 2300 7300
F 0 "#PWR0145" H 2300 7050 50  0001 C CNN
F 1 "GND" H 2305 7127 50  0000 C CNN
F 2 "" H 2300 7300 50  0001 C CNN
F 3 "" H 2300 7300 50  0001 C CNN
	1    2300 7300
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 7200 2600 7300
Wire Wire Line
	2600 6900 2600 7000
Wire Wire Line
	2300 6900 2300 7000
Wire Wire Line
	2300 7200 2300 7300
Text Notes 2150 6700 0    50   ~ 10
FT230 Decoupling
Text Notes 1950 7650 0    50   Italic 0
Note: place close to FT230.
Wire Wire Line
	1450 5500 1450 6000
$Comp
L power:GND #PWR0146
U 1 1 5ECB5ED8
P 1200 6000
F 0 "#PWR0146" H 1200 5750 50  0001 C CNN
F 1 "GND" H 1205 5827 50  0000 C CNN
F 2 "" H 1200 6000 50  0001 C CNN
F 3 "" H 1200 6000 50  0001 C CNN
	1    1200 6000
	1    0    0    -1  
$EndComp
Wire Wire Line
	1200 6000 1200 5900
Connection ~ 1200 5900
Wire Wire Line
	1200 5900 1350 5900
Text Notes 700  4500 0    50   ~ 10
USB Micro Port
Text HLabel 10650 3450 2    50   Input ~ 0
FTDI_TX
Text HLabel 10650 3550 2    50   Input ~ 0
FTDI_RX
$Comp
L radio-library:Conn_01x03 J2
U 1 1 5ECAF6FB
P 1250 1450
F 0 "J2" V 1123 1630 50  0000 L CNN
F 1 "Conn_01x03" V 1214 1630 50  0000 L CNN
F 2 "radio-footprints:Pin_Header_Straight_1x03_Pitch2.54mm" H 1250 1450 50  0001 C CNN
F 3 "~" H 1250 1450 50  0001 C CNN
	1    1250 1450
	0    1    1    0   
$EndComp
Text Label 1000 1200 2    50   ~ 10
3V3_REG
Wire Wire Line
	1450 1200 1350 1200
Wire Wire Line
	1350 1200 1350 1250
Wire Wire Line
	1150 1250 1150 1200
Wire Wire Line
	1150 1200 1000 1200
$Comp
L power:+3V3 #PWR0147
U 1 1 5ECC0115
P 1250 1100
F 0 "#PWR0147" H 1250 950 50  0001 C CNN
F 1 "+3V3" H 1265 1273 50  0000 C CNN
F 2 "" H 1250 1100 50  0001 C CNN
F 3 "" H 1250 1100 50  0001 C CNN
	1    1250 1100
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 1100 1250 1250
Text Label 1450 1200 0    50   ~ 10
3V3_EXT_PROTECTED
Text Notes 700  2650 0    50   ~ 10
3.3 V Reverse Polarity, Overcurrent,\nand ESD Protection
Text Notes 650  750  0    50   ~ 10
User Power Source Select
Text Notes 3500 2550 0    50   ~ 10
3.3 V Regulator
Text Label 3750 3000 2    50   ~ 10
USB_5V
Text Label 4950 3000 0    50   ~ 10
3V3_REG
$Comp
L power:GND #PWR0148
U 1 1 5ECCE889
P 4350 3550
F 0 "#PWR0148" H 4350 3300 50  0001 C CNN
F 1 "GND" H 4355 3377 50  0000 C CNN
F 2 "" H 4350 3550 50  0001 C CNN
F 3 "" H 4350 3550 50  0001 C CNN
	1    4350 3550
	1    0    0    -1  
$EndComp
Text Notes 4000 3950 0    50   Italic 0
AP7361: 3V3 @ 1A
$Comp
L radio-library:R_Small_US R17
U 1 1 5EC9C70F
P 7050 1750
F 0 "R17" H 6850 1800 50  0000 L CNN
F 1 "1k" H 6900 1700 50  0000 L CNN
F 2 "radio-footprints:R_0402" H 7050 1750 50  0001 C CNN
F 3 "~" H 7050 1750 50  0001 C CNN
	1    7050 1750
	1    0    0    -1  
$EndComp
$Comp
L radio-library:LED D2
U 1 1 5EC9C715
P 7050 1350
F 0 "D2" V 7100 1550 50  0000 R CNN
F 1 "3v3 Good (Yellow)" V 7000 2150 50  0000 R CNN
F 2 "radio-footprints:LED_0603_HandSoldering" H 7050 1350 50  0001 C CNN
F 3 "~" H 7050 1350 50  0001 C CNN
	1    7050 1350
	0    -1   -1   0   
$EndComp
Wire Wire Line
	7050 1500 7050 1650
Wire Wire Line
	7050 1200 7050 1050
$Comp
L power:+3V3 #PWR0149
U 1 1 5EC9EB5E
P 7050 1050
F 0 "#PWR0149" H 7050 900 50  0001 C CNN
F 1 "+3V3" H 7065 1223 50  0000 C CNN
F 2 "" H 7050 1050 50  0001 C CNN
F 3 "" H 7050 1050 50  0001 C CNN
	1    7050 1050
	1    0    0    -1  
$EndComp
Text Notes 6200 750  0    50   ~ 10
3v3 Good LED
Text Notes 700  7650 0    50   Italic 0
Note: place close to FT230.
$Comp
L power:GND #PWR0150
U 1 1 5ED671F3
P 7050 1950
F 0 "#PWR0150" H 7050 1700 50  0001 C CNN
F 1 "GND" H 7055 1777 50  0000 C CNN
F 2 "" H 7050 1950 50  0001 C CNN
F 3 "" H 7050 1950 50  0001 C CNN
	1    7050 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	7050 1850 7050 1950
$Comp
L radio-library:DMN2056U Q1
U 1 1 5ED4C613
P 1350 3200
F 0 "Q1" H 1555 3246 50  0000 L CNN
F 1 "DMN2056U" H 1555 3155 50  0000 L CNN
F 2 "radio-footprints:SOT-23" H 1550 3125 50  0001 L CIN
F 3 "https://www.diodes.com/assets/Datasheets/DMN2056U.pdf" H 1350 3200 50  0001 L CNN
	1    1350 3200
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0130
U 1 1 5ED4CA3A
P 1450 3500
F 0 "#PWR0130" H 1450 3250 50  0001 C CNN
F 1 "GND" H 1455 3327 50  0000 C CNN
F 2 "" H 1450 3500 50  0001 C CNN
F 3 "" H 1450 3500 50  0001 C CNN
	1    1450 3500
	1    0    0    -1  
$EndComp
$Comp
L radio-library:Conn_01x05 J?
U 1 1 5ED51C7E
P 3850 1350
AR Path="/5EC971DB/5ED51C7E" Ref="J?"  Part="1" 
AR Path="/5EC971ED/5ED51C7E" Ref="J1"  Part="1" 
F 0 "J1" H 3770 925 50  0000 C CNN
F 1 "Conn_01x05" H 3900 1000 50  0000 C CNN
F 2 "radio-footprints:Pin_Header_Straight_1x05_Pitch2.54mm" H 3850 1350 50  0001 C CNN
F 3 "~" H 3850 1350 50  0001 C CNN
	1    3850 1350
	-1   0    0    1   
$EndComp
Wire Wire Line
	4050 1250 4150 1250
Text Notes 3500 750  0    50   ~ 10
External Power and Debug Header
Text Notes 3500 2250 0    50   Italic 0
Two GNDs to allow ST-Link connection while being powered\nfrom a bench supply.\n\nPins 2-5 are in the same order as cheap ST-Link connector. \nBoard can be powered from ST-Link 3V3 for initial bringup.\n\nPower input is reverse polarity protected.
Text GLabel 4550 1350 2    50   BiDi ~ 0
SWDIO
Text GLabel 4550 1450 2    50   Output ~ 0
SWCLK
Text Label 4150 1250 0    50   ~ 10
3V3_EXT
Text Label 4150 1150 0    50   ~ 10
GND_EXT
Text Label 4150 1550 0    50   ~ 10
GND_EXT
Wire Wire Line
	4050 1350 4550 1350
Wire Wire Line
	4150 1150 4050 1150
Wire Wire Line
	4050 1450 4550 1450
Wire Wire Line
	4150 1550 4050 1550
Text Label 800  3200 0    50   ~ 10
3V3_EXT
Wire Wire Line
	1150 3200 800  3200
Text Label 1450 2900 0    50   ~ 10
GND_EXT
Wire Wire Line
	1450 2900 1450 3000
Wire Wire Line
	1450 3500 1450 3400
$Comp
L radio-library:T3V3S5 D5
U 1 1 5ED90EF6
P 2500 3200
F 0 "D5" V 2454 3279 50  0000 L CNN
F 1 "T3V3S5" V 2545 3279 50  0000 L CNN
F 2 "radio-footprints:D_SOD-523" H 2500 3000 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/ds31112.pdf" H 2450 3200 50  0001 C CNN
	1    2500 3200
	0    1    1    0   
$EndComp
Text Label 2150 3000 0    50   ~ 10
3V3_EXT
$Comp
L power:GND #PWR0131
U 1 1 5ED910E4
P 2500 3500
F 0 "#PWR0131" H 2500 3250 50  0001 C CNN
F 1 "GND" H 2505 3327 50  0000 C CNN
F 2 "" H 2500 3500 50  0001 C CNN
F 3 "" H 2500 3500 50  0001 C CNN
	1    2500 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	2150 3000 2500 3000
Wire Wire Line
	2500 3000 2500 3050
Wire Wire Line
	2500 3500 2500 3350
$Comp
L radio-library:Polyfuse_Small F1
U 1 1 5ED96AAC
P 1300 4100
F 0 "F1" V 1095 4100 50  0000 C CNN
F 1 "350 mA" V 1186 4100 50  0000 C CNN
F 2 "radio-footprints:R_1206" H 1350 3900 50  0001 L CNN
F 3 "~" H 1300 4100 50  0001 C CNN
	1    1300 4100
	0    1    1    0   
$EndComp
$Comp
L radio-library:Polyfuse_Small F2
U 1 1 5ED96B9B
P 3100 4900
F 0 "F2" V 2850 4900 50  0000 C CNN
F 1 "350 mA" V 2950 4900 50  0000 C CNN
F 2 "radio-footprints:R_1206" H 3150 4700 50  0001 L CNN
F 3 "~" H 3100 4900 50  0001 C CNN
	1    3100 4900
	0    1    1    0   
$EndComp
Connection ~ 2400 4900
Wire Wire Line
	2400 4900 2600 4900
Wire Wire Line
	3200 4900 3300 4900
Text Label 1550 4100 0    50   ~ 10
3V3_EXT_PROTECTED
Text Label 850  4100 0    50   ~ 10
3V3_EXT
Wire Wire Line
	1550 4100 1400 4100
Wire Wire Line
	1200 4100 850  4100
Wire Notes Line
	7400 600  9000 600 
Wire Notes Line
	9000 600  9000 2200
Wire Notes Line
	9000 2200 7400 2200
Wire Notes Line
	7400 2200 7400 600 
Wire Notes Line
	7300 600  6100 600 
Wire Notes Line
	6100 600  6100 2200
Wire Notes Line
	6100 2200 7300 2200
Wire Notes Line
	7300 2200 7300 600 
Wire Notes Line
	3400 600  6000 600 
Wire Notes Line
	6000 600  6000 2300
Wire Notes Line
	6000 2300 3400 2300
Wire Notes Line
	3400 2300 3400 600 
Wire Notes Line
	600  600  3300 600 
Wire Notes Line
	3300 600  3300 2300
Wire Notes Line
	3300 2300 600  2300
Wire Notes Line
	600  2300 600  600 
Wire Notes Line
	600  2400 3300 2400
Wire Notes Line
	3300 2400 3300 4200
Wire Notes Line
	3300 4200 600  4200
Wire Notes Line
	600  4200 600  2400
Wire Notes Line
	600  4300 3700 4300
Wire Notes Line
	3700 4300 3700 6300
Wire Notes Line
	3700 6300 600  6300
Wire Notes Line
	600  6300 600  4300
Wire Notes Line
	3400 2400 5300 2400
Wire Notes Line
	600  6550 1800 6550
Wire Notes Line
	1800 6550 1800 7700
Wire Notes Line
	1800 7700 600  7700
Wire Notes Line
	600  7700 600  6550
Wire Notes Line
	1900 7700 1900 6550
Wire Notes Line
	3050 6550 3050 7700
Wire Notes Line
	1900 7700 3050 7700
Wire Notes Line
	1900 6550 3050 6550
Text Notes 6950 2600 0    50   ~ 10
FTDI USB-Serial
Wire Notes Line
	11050 2400 6850 2400
Wire Notes Line
	6850 2400 6850 5100
Wire Notes Line
	6850 5100 11050 5100
Wire Notes Line
	11050 5100 11050 2400
$Comp
L radio-library:AP7361 U2
U 1 1 5ED4CD85
P 4300 2850
F 0 "U2" H 4350 3065 50  0000 C CNN
F 1 "AP7361" H 4350 2974 50  0000 C CNN
F 2 "radio-footprints:SOT-23" H 4950 3050 50  0001 C CNN
F 3 "" H 4950 3050 50  0001 C CNN
	1    4300 2850
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3450 4350 3500
Wire Wire Line
	3750 3000 3850 3000
Wire Wire Line
	4800 3000 4950 3000
$Comp
L radio-library:C_Small C22
U 1 1 5ED5753A
P 4800 3300
F 0 "C22" H 4892 3346 50  0000 L CNN
F 1 "4.7 uF" H 4892 3255 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 4800 3300 50  0001 C CNN
F 3 "~" H 4800 3300 50  0001 C CNN
	1    4800 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	4350 3500 4800 3500
Wire Wire Line
	4800 3500 4800 3400
Connection ~ 4350 3500
Wire Wire Line
	4350 3500 4350 3550
Wire Wire Line
	4800 3200 4800 3000
Connection ~ 4800 3000
$Comp
L radio-library:C_Small C25
U 1 1 5ED5EC9C
P 3850 3350
F 0 "C25" H 3942 3396 50  0000 L CNN
F 1 "4.7 uF" H 3942 3305 50  0000 L CNN
F 2 "radio-footprints:C_0402" H 3850 3350 50  0001 C CNN
F 3 "~" H 3850 3350 50  0001 C CNN
	1    3850 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	3850 3250 3850 3000
Connection ~ 3850 3000
Wire Wire Line
	3850 3000 3900 3000
Wire Wire Line
	3850 3450 3850 3500
Wire Wire Line
	3850 3500 4350 3500
Wire Notes Line
	5300 4200 3400 4200
Wire Notes Line
	3400 2400 3400 4200
Wire Notes Line
	5300 2400 5300 4200
$Comp
L power:GND #PWR0174
U 1 1 5EDC8591
P 8800 4750
F 0 "#PWR0174" H 8800 4500 50  0001 C CNN
F 1 "GND" H 8805 4577 50  0000 C CNN
F 2 "" H 8800 4750 50  0001 C CNN
F 3 "" H 8800 4750 50  0001 C CNN
	1    8800 4750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8800 4550 8800 4750
$EndSCHEMATC
